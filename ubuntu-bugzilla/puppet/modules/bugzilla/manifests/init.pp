# puppet/manifests/modules/bugzilla/manifests/init.pp

class bugzilla {

    exec { 'download-bugzilla':
        command => "/usr/bin/wget https://ftp.mozilla.org/pub/mozilla.org/webtools/bugzilla-4.4.9.tar.gz",
        unless => "/bin/readlink -e /tmp/bugzilla-4.4.9.tar.gz",
        cwd => "/tmp/"
    }

    exec { 'untar-bugzilla':
        command => "/bin/tar -xvf bugzilla-4.4.9.tar.gz",
        unless => "/bin/readlink -e /tmp/bugzilla-4.4.9",
        cwd => "/tmp/",
        require => Exec['download-bugzilla']
    }

    exec { 'copy-bugzilla':
        command => "/bin/cp -R /tmp/bugzilla-4.4.9 /home/vagrant/bugzilla",
        unless => "/bin/readlink -e /home/vagrant/bugzilla",
        require => Exec['untar-bugzilla']
    }

    exec { 'install-perl-modules':
        command => "/usr/bin/perl install-module.pl --all",        
        cwd => "/home/vagrant/bugzilla",
        timeout => 1800,
        require => [
            Exec['copy-bugzilla'],
            Exec['untar-bugzilla-testopia'],
            Package['perl']
        ]
    }

    file { 'bugzilla-localconfig':
        path => '/home/vagrant/bugzilla/localconfig',
        source => 'puppet:///modules/bugzilla/localconfig',
        ensure => file,
        require => [
            Exec['copy-bugzilla']
        ],
    }

    file { 'bugzilla-responses':
        path => '/tmp/bugzilla-responses',
        source => 'puppet:///modules/bugzilla/bugzilla-responses',
        ensure => file,
        require => [
            Exec['copy-bugzilla']
        ],
    }

    file { '/home/vagrant/bugzilla/extensions':
        ensure => 'directory',
        require => [
           Exec['copy-bugzilla']
        ]
    }

    exec { 'checksetup':
        command => "/home/vagrant/bugzilla/checksetup.pl /tmp/bugzilla-responses",
        cwd => "/home/vagrant/bugzilla",
        timeout => 1800,
        require => [
            File['bugzilla-responses'],
            Exec['untar-bugzilla-testopia'],            
            Package['perl'],
            Mysql::Db['bugzilla']
        ]
    }

    exec { 'download-bugzilla-testopia':
        command => "/usr/bin/wget https://ftp.mozilla.org/pub/mozilla.org/webtools/testopia/testopia-2.5-BUGZILLA-4.2.tar.gz",
        unless => "/bin/readlink -e /tmp/testopia-2.5-BUGZILLA-4.2.tar.gz",
        cwd => "/tmp/"
    }

    exec { 'untar-bugzilla-testopia':
        command => "/bin/tar -xvf testopia-2.5-BUGZILLA-4.2.tar.gz",
        unless => "/bin/readlink -e /home/vagrant/bugzilla/extensions/Testopia",
        cwd => "/home/vagrant/bugzilla/",
        require => Exec['copy-bugzilla-testopia']
    }

    exec { 'copy-bugzilla-testopia':
        command => "/bin/cp -R /tmp/testopia-2.5-BUGZILLA-4.2.tar.gz /home/vagrant/bugzilla/testopia-2.5-BUGZILLA-4.2.tar.gz",
        unless => "/bin/readlink -e /home/vagrant/bugzilla/testopia-2.5-BUGZILLA-4.2.tar.gz",
        require => [
            Exec['download-bugzilla-testopia'],
            Exec['copy-bugzilla']
        ]
    }

    # Add a vhost template
    file { 'vagrant-bugzilla-conf':
        path => '/etc/apache2/sites-available/bugzilla.conf',
        ensure => file,
        require => Package['apache2'],
        source => 'puppet:///modules/bugzilla/bugzilla.conf',
    }

    # Symlink our vhost in sites-enabled to enable it
    file { 'vagrant-apache-bugzilla-enable':
        path => '/etc/apache2/sites-enabled/bugzilla.conf',
        target => '/etc/apache2/sites-available/bugzilla.conf',
        ensure => link,
        notify => Service['apache2'],
        require => [
            File['vagrant-bugzilla-conf']
        ],
    }
}