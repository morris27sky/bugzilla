# puppet/modules/mysql/install-mysql/manifests/init.pp
class install-mysql {

    class { '::mysql::server':
        root_password => 'test1234'
    }

    mysql::db { 'bugzilla':
        user     => 'bugzilla',
        password => 'test1234',
        host     => 'localhost',
        grant    => ['INDEX', 'INSERT', 'SELECT', 'UPDATE', 'ALTER', 'CREATE', 'DELETE'],
        require  => Class['::mysql::server']
    }

    # Install the mysql-server
    package { ['mysql-client-5.5']:
        ensure => present,
        require => Exec['apt-get update'],
    }
}
