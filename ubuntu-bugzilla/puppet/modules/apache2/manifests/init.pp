# puppet/manifests/modules/apache/manifests/init.pp

class apache2 {
    
    package { 'apache2' :
        name => 'apache2',
        ensure => present,
        require => Exec['apt-get update']
    }

    package { 'apache2-utils':
        name => 'apache2-utils',
        ensure => present,
        require => Package['apache2']
    }

    package { 'libapache2-mod-perl2':
        name => 'libapache2-mod-perl2',
        ensure => present,
        require => Package['apache2']
    }

    # Make sure that the apache service is running
    service { 'apache2':
        ensure => running,
        require => Package['apache2']
    }

    exec { "enable-mod-expires" :
        command => "/usr/sbin/a2enmod expires",
        unless => "/bin/readlink -e /etc/apache2/mods-enabled/expires.load",
        notify => Service['apache2'],
        require => Package['apache2-utils']
    }

    exec { "enable-mod-headers" :
        command => "/usr/sbin/a2enmod headers",
        unless => "/bin/readlink -e /etc/apache2/mods-enabled/headers.load",
        notify => Service['apache2'],
        require => Package['apache2-utils']
    }

    exec { "enable-mod-rewrite" :
        command => "/usr/sbin/a2enmod rewrite",
        unless => "/bin/readlink -e /etc/apache2/mods-enabled/rewrite.load",
        notify => Service['apache2'],
        require => Package['apache2-utils']
    }

    exec { "enable-mod-perl" :
        command => "/usr/sbin/a2enmod perl",
        unless => "/bin/readlink -e /etc/apache2/mods-enabled/perl.load",
        notify => Service['apache2'],
        require => Package['libapache2-mod-perl2']
    }

    exec { "enable-mod-cgi" :
        command => "/usr/sbin/a2enmod cgi",
        unless => "/bin/readlink -e /etc/apache2/mods-enabled/cgid.load",
        notify => Service['apache2'],
        require => Package['apache2']
    }

    # Disable the default apache vhost
    file { 'default-apache-disable':
        path => '/etc/apache2/sites-enabled/000-default.conf',
        ensure => absent,
        require => Package['apache2'],
        notify  => Service["apache2"]
    }
}