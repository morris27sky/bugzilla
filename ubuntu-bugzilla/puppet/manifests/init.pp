# puppet/manifests/init.pp

exec { 'apt-get update':
  path => '/usr/bin',
}

package { ['vim', 'git', 'perl', 'g++', 'libgd-dev']:
  ensure => present,
}

include install-mysql, apache2, bugzilla