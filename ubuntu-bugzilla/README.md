# ubuntu-bugzilla
Bugzilla Virtual Machine set up using Vagrant and Puppet

# Pre req

- Vagrant 1.6.3 or higher
- Virtualbox 4.3

# Setup

- git clone https://bitbucket.org/morris27sky/bugzilla
- setup ssh keys (https://help.github.com/articles/generating-ssh-keys/)
- cd to repo and run 'Vagrant up'

# If you get an error to do with 'ubuntu/trusty64', run the following

vagrant box add --insecure 'ubuntu/trusty64' https://atlas.hashicorp.com/ubuntu/boxes/trusty64/versions/14.04/providers/virtualbox.box

# Admin user
- Admin user has been created.. username: admin@example.com -password: password